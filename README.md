spring-linking
==============

Whiteboard project for exploring link creation in a Spring Web App

Why?
==============
Spring-based web projects that try to be hypertext-driven, tend to manage their URI space in 
multiple places - in the Controller definition itself and in the place where the application code
is crafting the links. 

Doing things more that once is a burden.

So?
==============
We can use reflection to inspect the Controllers params and create a URI Template for us.  But then
we still need to figure out a way to bind to them when we're creating the links in application
code.  So, we introduce the @Resource annotation that accepts a NamedResource.  

Suppose we  have a controller like:
```Java
    @Resource(NamedResource.Search)
    @RequestMapping("/search")
    public ResponseEntity<Object> search(
      @RequestParam(value="q") String query)  {  ...
```
      
Then, we can create links to it like:

```Java
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("q", "apple");

    String uri = Linker.on(NamedResource.Search).expand(params);
```    
Which expands to:
    /search?q=apple
    
Overriding
============
Sometimes, you want to set up a Map of params and then selectively override only one or two values.  

Suppose we had a controller like this:
```Java
    @Resource(NamedResource.Search)
    @RequestMapping("/search")
    public ResponseEntity<Object> search(
      @RequestParam(value="q") String query,
      @RequestParam(value="page") Integer page,
      @RequestParam(value="pageSize") Integer pageSize)  {  ...
```
We might want to craft links to the current page, next, and previous pages - only overriding the page values.
```Java
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("q", "apple");
    params.put("page", 2);
    params.put("pageSize", 10);

    String uri = Linker.on(NamedResource.Search).expand(params);
    //returns /search?q=apple&page=2&pageSize=10
    
    String uri = Linker.on(NamedResource.Search).override("page", 1).expand(params);
    //returns /search?q=apple&page=1&pageSize=10
    
    String uri = Linker.on(NamedResource.Search).override("page", 3).expand(params);
    //returns /search?q=apple&page=3&pageSize=10
```  
Ignoring
============
Sometimes, you want to set up a Map of params and then selectively ignore one or two optional ones.

Suppose we had a controller like this:
```Java
    @Resource(NamedResource.SearchWithinId)
    @RequestMapping("/{id}/search")
    public ResponseEntity<Object> searchWithinId(
        @PathVariable(value="id") Integer id,
        @RequestParam(value="facet", required=false) List<String> facet,
        @RequestParam(value="q") String query)  {...
        
    String uri = Linker.on(NamedResource.SearchWithinId).ignore("facet").expand(params);
    //returns /1/search?q=apple
```
    
