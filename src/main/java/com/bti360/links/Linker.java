package com.bti360.links;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.MethodParameter;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.damnhandy.uri.template.UriTemplate;

public class Linker {
  private static final Map<NamedResource, String> templateMap = new HashMap<NamedResource, String>();
  //TODO: Surely theres a way to snag this from Spring?
  private static final String WEB_CONTEXT = "/service";
  private String template;
  private String[] excludeParams = ArrayUtils.EMPTY_STRING_ARRAY;
  private Map<String, Object> overrides = new HashMap<String, Object>();

  private Linker(String uriTemplate) {
    this.template = uriTemplate;
  } 

  public Linker ignore(String... exclude) {
    this.excludeParams = exclude;
    return this;
  }
  
  public Linker override(String key, Object value) {
    overrides.put(key, value);
    return this;
  }

  public String expand(Map<String, Object> params) {
    Map<String, Object> p = new HashMap<String, Object>(params);
    
    for(String exclude: excludeParams) {
      p.remove(exclude);
    }
    p.putAll(overrides);
    return UriTemplate.fromTemplate(template).expand(p);
  }

  public static Linker on(NamedResource resource) {
    return new Linker(templateMap.get(resource));
  }
  
  public static void main(String[] args) {
    System.out.println("Found resources...");
    for(Map.Entry<NamedResource, String> tmpls: templateMap.entrySet()) {
      System.out.println("Resource[" + tmpls.getKey() + "] -> [" + tmpls.getValue() + "]");
    }
  }

  static {
    ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
        false);
    scanner.addIncludeFilter(new AnnotationTypeFilter(Controller.class));

    List<Class<?>> controllers = findControllers(scanner);

    for (Class<?> controller : controllers) {
      Method[] methods = controller.getDeclaredMethods();
      for (Method m : methods) {
        RequestMapping requestMapping = m.getAnnotation(RequestMapping.class);
        Resource resource = m.getAnnotation(Resource.class);

        String baseTemplate = WEB_CONTEXT;

        if ((requestMapping == null) || (resource == null)){
          continue;
        } else {
          baseTemplate += requestMapping.value()[0];
        }

        if (templateMap.containsKey(resource.value())) {
          String previous = templateMap.get(resource.value());
          throw new RuntimeException(
              "Invalid resource definition - resource mapped to multiple URIs["
                  + resource.value() + "] previousl mapped to [" + previous
                  + "] duplicate found[" + requestMapping.value()[0] + "]");
        }

        String qParams = createQueryStringTemplate(m, m.getParameterTypes().length);
        templateMap.put(resource.value(), baseTemplate + "{?" + qParams + "}");
      }
    }
  }

  private static List<Class<?>> findControllers(
      ClassPathScanningCandidateComponentProvider scanner) {
    List<Class<?>> controllers = new ArrayList<Class<?>>();
    for (BeanDefinition d : scanner.findCandidateComponents("com.bti360.controllers")) {
      try {
        controllers.add(Class.forName(d.getBeanClassName()));
      } catch (ClassNotFoundException e) {
        throw new RuntimeException(e);
      }
    }
    return controllers;
  }

  private static String createQueryStringTemplate(Method m, int paramSize) {
    StringBuffer qParams = new StringBuffer();
    
    for(int i = 0; i < paramSize; i++) {
      MethodParameter param = MethodParameter.forMethodOrConstructor(m,  i);
      Annotation a = param.getParameterAnnotation(RequestParam.class);
 
      if(a != null) {
        RequestParam rp = (RequestParam)a;
        
        String postModifier = ",";
        
        if(param.getParameterType().isAssignableFrom(List.class)) {
          postModifier = "*,";
        }
   
        qParams.append(rp.value()).append(postModifier) ;
      }
    }
    return qParams.toString();
  }

}
