package com.bti360.links;

public enum NamedResource {
  Search, 
  SearchWithinId, 
  SearchWithPaging

}
