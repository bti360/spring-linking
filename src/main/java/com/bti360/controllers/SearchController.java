package com.bti360.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bti360.links.NamedResource;
import com.bti360.links.Resource;

@Controller
public class SearchController {

  @Resource(NamedResource.Search)
  @RequestMapping("/search")
  public ResponseEntity<Object> search(
      @RequestParam(value="q") String query)  {
    return null;
  }
  
  @Resource(NamedResource.SearchWithinId)
  @RequestMapping("/{id}/search")
  public ResponseEntity<Object> searchWithinId(
      @PathVariable(value="id") Integer id,
      @RequestParam(value="facet", required=false) List<String> facet,
      @RequestParam(value="q") String query)  {
    return null;
  } 
  
  @Resource(NamedResource.SearchWithPaging)
  @RequestMapping("/search")
  public ResponseEntity<Object> searchWithinId(
      @PathVariable(value="id") Integer id,
      @RequestParam(value="page") Integer page,
      @RequestParam(value="pageSize", required=false) Integer pageSize,
      @RequestParam(value="facet", required=false) List<String> facet,
      @RequestParam(value="q") String query)  {
    return null;
  } 
}
