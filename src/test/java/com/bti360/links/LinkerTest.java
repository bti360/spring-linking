package com.bti360.links;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;



public class LinkerTest {
 private static Map<String, Object> params = new HashMap<String, Object>();
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    List<String> facets = new ArrayList<String>();
    facets.add("cucumber");
    facets.add("squash");
  
    params.put("q", "apple");
    params.put("page", 2);
    params.put("pageSize", 10);
    params.put("id", 1);
    params.put("facet", facets);
  }

  @Test
  public void testBasicExpansion() {
    String url = Linker.on(NamedResource.Search).expand(params);
    assertEquals("Basic params should expand properly", "/service/search?q=apple", url);
  }

  @Test
  public void testOverride() {
    String url = Linker.on(NamedResource.Search).override("q","banana").expand(params);
    assertEquals("We should be able to locally override the params", "/service/search?q=banana", url);
  }
  
  @Test
  public void testExpandsLists() {
    String url = Linker.on(NamedResource.SearchWithinId).expand(params);
    
    assertEquals("We should be able to selectively ignore params", "/service/1/search?facet=cucumber&facet=squash&q=apple", url);
  }
  @Test
  public void testMultipleParams() {
    String url = Linker.on(NamedResource.SearchWithPaging).expand(params);
    
    assertEquals("We should be able to selectively ignore params", "/service/search?page=2&pageSize=10&facet=cucumber&facet=squash&q=apple", url);
  }
  
  @Test
  public void testIgnoringMultipleParams() {
    String url = Linker.on(NamedResource.SearchWithPaging).ignore("facet","page","pageSize").expand(params);
    
    assertEquals("We should be able to selectively ignore params", "/service/search?q=apple", url);
  }  
  
  @Test
  public void testIgnores() {
    String url = Linker.on(NamedResource.SearchWithinId).ignore("facet").expand(params);
    
    assertEquals("We should be able to selectively ignore params", "/service/1/search?q=apple", url);
  }
}
